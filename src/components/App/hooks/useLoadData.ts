import { Dispatch, useCallback, useEffect } from "react";
import { Action } from "../../../reducer/Action";
import { State } from "../../../reducer/State";
import { searchGithubApi, searchStarred } from "../../../search";
import { SearchParams } from "../../../search/types";

const useLoadData = (state: State, dispatch: Dispatch<Action>) => {
	const { searchParams, starredMap } = state;
	const { showStarred } = searchParams;

	const searchGithub = useCallback(
		async (params: SearchParams) => {
			dispatch({ type: "ASYNC_ACTION_START" });
			const result = await searchGithubApi(params);
			dispatch({
				type: "SET_SEARCH_RESULTS",
				items: result.items,
				totalCount: result.total_count,
			});
			dispatch({ type: "ASYNC_ACTION_END" });
		},
		[dispatch],
	);

	useEffect(() => {
		if (showStarred) {
			return;
		}

		searchGithub(searchParams);
	}, [searchGithub, searchParams, showStarred]);

	useEffect(() => {
		if (!showStarred) {
			return;
		}

		const result = searchStarred(searchParams, starredMap);
		dispatch({
			type: "SET_SEARCH_RESULTS",
			items: result.items,
			totalCount: result.total_count,
		});
	}, [dispatch, searchParams, showStarred, starredMap]);
};

export default useLoadData;
