import { Dispatch, useEffect } from "react";
import { Action } from "../../../reducer/Action";
import { State } from "../../../reducer/State";

const LOCAL_STORAGE_KEY = "starred_ids";

const useStarredPersistence = (state: State, dispatch: Dispatch<Action>) => {
	useEffect(() => {
		const idsString =
			localStorage.getItem(LOCAL_STORAGE_KEY) || JSON.stringify([]);
		const items = JSON.parse(idsString);
		dispatch({ type: "TOGGLE_STARRED", items });
	}, [dispatch]);

	const { starredMap } = state;
	useEffect(() => {
		const items = Array.from(starredMap, ([_id, item]) => item);
		localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(items));
	}, [starredMap]);
};

export default useStarredPersistence;
