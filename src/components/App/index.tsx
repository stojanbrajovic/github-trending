import { AppBar as MuiAppBar, List } from "@mui/material";
import { styled } from "@mui/system";
import { useMemo, useReducer } from "react";
import { context } from "../../context";
import reducer from "../../reducer";
import { initialState } from "../../reducer/State";
import LoadingIndicator from "../LoadingIndicator";
import SearchParams from "../SearchParams";
import SingleItem from "../SingleItem";
import useLoadData from "./hooks/useLoadData";
import useStarredPersistence from "./hooks/useStarredPersistence";

const OuterDiv = styled("div")`
	display: flex;
	justify-content: center;
`;

const InnerDiv = styled("div")`
	max-width: 820px;
	width: 100%;
	padding: 10px;
`;

const AppBar = styled(MuiAppBar)`
	padding: 10px;
`;

function App() {
	const [state, dispatch] = useReducer(reducer, initialState);

	useLoadData(state, dispatch);
	useStarredPersistence(state, dispatch);

	const { asyncActionsActive, items, starredMap } = state;
	const itemsList = useMemo(
		() => (
			<List dense>
				{items.map((item) => (
					<SingleItem
						item={item}
						key={item.id}
						starredMap={starredMap}
						dispatch={dispatch}
					/>
				))}
			</List>
		),
		[items, starredMap],
	);

	return (
		<>
			<LoadingIndicator loading={asyncActionsActive > 0} />
			<context.Provider value={{ state, dispatch }}>
				<OuterDiv>
					<InnerDiv>
						<AppBar position="sticky" color="default">
							<SearchParams />
						</AppBar>
						{itemsList}
					</InnerDiv>
				</OuterDiv>
			</context.Provider>
		</>
	);
}

export default App;
