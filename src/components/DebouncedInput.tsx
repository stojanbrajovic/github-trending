import { TextField, TextFieldProps } from "@mui/material";
import { ChangeEvent, useCallback, useEffect, useMemo, useState } from "react";
import { debounce } from "ts-debounce";

const DEBOUNCE_INTERVAL = 300;

type Properties = Omit<TextFieldProps, "onChange" | "value"> & {
	onChange: (val: string) => void;
	debounceInterval?: number;
	value: string;
};

function DebouncedInput(props: Properties) {
	const [internalValue, setInternalValue] = useState("");
	const { onChange, debounceInterval, ...textFieldProps } = props;
	const debouncedOnChange = useMemo(
		() =>
			debounce(
				(value: string) => onChange(value),
				debounceInterval || DEBOUNCE_INTERVAL,
			),
		[debounceInterval, onChange],
	);

	const onTextFieldChange = useCallback(
		(event: ChangeEvent<HTMLInputElement>) => {
			const { value } = event.currentTarget;
			setInternalValue(value);
			debouncedOnChange(value);
		},
		[debouncedOnChange],
	);

	const externalValue = props.value || "";
	useEffect(() => {
		setInternalValue(externalValue);
	}, [externalValue]);

	return (
		<TextField
			size="small"
			{...textFieldProps}
			onChange={onTextFieldChange}
			value={internalValue}
		/>
	);
}

export default DebouncedInput;
