import { Autocomplete, styled, TextField } from "@mui/material";
import { useCallback, useContext, useEffect, useMemo } from "react";
import { context } from "../context";
import fetchLanguages from "../search/fetchLanguages";

const Input = styled(TextField)`
	width: 220px;
	margin-left: 10px;
`;

function LanguagesSearch() {
	const { state, dispatch } = useContext(context);
	const { availableLanguages } = state;

	useEffect(() => {
		const load = async () => {
			dispatch({ type: "ASYNC_ACTION_START" });
			const languages = await fetchLanguages();
			dispatch({ type: "SET_AVAILABLE_LANGUAGES", languages });
			dispatch({ type: "ASYNC_ACTION_END" });
		};
		load();
	}, [dispatch]);

	const options = useMemo(
		() => Object.keys(availableLanguages),
		[availableLanguages],
	);

	const onChange = useCallback(
		(_event, value: string | null) => {
			dispatch({
				type: "CHANGE_SEARCH_PARAMS",
				params: { language: value || "" },
			});
		},
		[dispatch],
	);

	return (
		<Autocomplete
			options={options}
			onChange={onChange}
			renderInput={(params) => (
				<Input {...params} label="Filter by language" size="small" />
			)}
		/>
	);
}

export default LanguagesSearch;
