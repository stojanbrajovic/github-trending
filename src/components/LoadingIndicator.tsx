import { CircularProgress, Backdrop } from "@mui/material";

type Properties = {
	loading: boolean;
};

function LoadingIndicator(props: Properties) {
	return (
		<Backdrop
			open={props.loading}
			sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
		>
			<CircularProgress color="inherit" />
		</Backdrop>
	);
}

export default LoadingIndicator;
