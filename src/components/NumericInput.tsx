import { useCallback } from "react";
import { TextFieldProps } from "@mui/material";
import DebouncedInput from "./DebouncedInput";

type Properties = Omit<TextFieldProps, "type" | "value" | "onChange"> & {
	value: number;
	onChange: (val: number) => void;
};

function NumericInput(props: Properties) {
	const { onChange, value, ...otherProps } = props;

	const onTextFieldChange = useCallback(
		(value: string) => {
			onChange(+value);
		},
		[onChange],
	);

	return (
		<DebouncedInput
			{...otherProps}
			value={value + ""}
			type="number"
			onChange={onTextFieldChange}
		/>
	);
}

export default NumericInput;
