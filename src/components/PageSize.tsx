import {
	FormControl,
	InputLabel,
	Select as MuiSelect,
	MenuItem,
	SelectChangeEvent,
	styled,
} from "@mui/material";
import { useCallback, useContext } from "react";
import { context } from "../context";

const OPTIONS = [10, 20, 50, 100];

const Select = styled(MuiSelect)`
	width: 100px;
`;

function PageSize() {
	const { state, dispatch } = useContext(context);

	const { pageSize } = state.searchParams;

	const onChange = useCallback(
		(event: SelectChangeEvent<unknown>) => {
			dispatch({
				type: "CHANGE_SEARCH_PARAMS",
				params: { pageSize: +(event.target.value + "") },
			});
		},
		[dispatch],
	);

	return (
		<FormControl>
			<InputLabel>Page Size</InputLabel>
			<Select
				size="small"
				value={pageSize}
				label="Page Size"
				onChange={onChange}
			>
				{OPTIONS.map((o) => (
					<MenuItem value={o} key={o}>
						{o}
					</MenuItem>
				))}
			</Select>
		</FormControl>
	);
}

export default PageSize;
