import { Star, StarBorder } from "@mui/icons-material";
import { Button, InputAdornment, styled } from "@mui/material";
import { useCallback, useContext } from "react";
import { context } from "../context";
import DebouncedInput from "./DebouncedInput";
import LanguagesSearch from "./LanguagesSearch";
import NumericInput from "./NumericInput";
import PageSize from "./PageSize";

const ContainerDiv = styled("div")`
	display: flex;
	justify-content: space-between;
`;

const BottonContainerDiv = styled(ContainerDiv)`
	margin-top: 10px;
`;

const PageInput = styled(NumericInput)`
	width: 150px;
`;

function SearchParams() {
	const { state, dispatch } = useContext(context);

	const { totalCount, searchParams } = state;
	const { page, pageSize, filter, showStarred } = searchParams;

	const prevPageClick = useCallback(() => {
		dispatch({ type: "CHANGE_SEARCH_PARAMS", params: { page: page - 1 } });
	}, [dispatch, page]);

	const nextPageClick = useCallback(() => {
		dispatch({ type: "CHANGE_SEARCH_PARAMS", params: { page: page + 1 } });
	}, [dispatch, page]);

	const maxPage = Math.ceil(totalCount / pageSize);
	const onPageChange = useCallback(
		(newPage: number) => {
			if (newPage < 1) {
				return;
			}

			if (newPage > maxPage) {
				return;
			}

			dispatch({ type: "CHANGE_SEARCH_PARAMS", params: { page: newPage } });
		},
		[dispatch, maxPage],
	);

	const onFilterChange = useCallback(
		(newFilter: string) => {
			dispatch({ type: "CHANGE_SEARCH_PARAMS", params: { filter: newFilter } });
		},
		[dispatch],
	);

	const toggleStar = useCallback(() => {
		dispatch({
			type: "CHANGE_SEARCH_PARAMS",
			params: { showStarred: !showStarred },
		});
	}, [dispatch, showStarred]);

	return (
		<>
			<ContainerDiv>
				<DebouncedInput
					label="Filter by Name"
					value={filter}
					onChange={onFilterChange}
					fullWidth
				/>
				<LanguagesSearch />
			</ContainerDiv>
			<BottonContainerDiv>
				<div>
					<Button disabled={page === 1} onClick={prevPageClick}>
						{"<"}
					</Button>
					<PageInput
						label="Page Number"
						value={page}
						onChange={onPageChange}
						InputProps={{
							endAdornment: (
								<InputAdornment position="end">/ {maxPage}</InputAdornment>
							),
						}}
					/>
					<Button
						disabled={page === maxPage || maxPage === 0}
						onClick={nextPageClick}
					>
						{">"}
					</Button>
					<PageSize />
				</div>
				<Button
					variant="contained"
					endIcon={showStarred ? <Star /> : <StarBorder />}
					onClick={toggleStar}
				>
					{showStarred ? "Show Github repos" : "Show Starred repos"}
				</Button>
			</BottonContainerDiv>
		</>
	);
}

export default SearchParams;
