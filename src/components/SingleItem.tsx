import { Dispatch, useCallback } from "react";
import { Button, ListItem, ListItemButton, ListItemText } from "@mui/material";
import { Star, StarBorder } from "@mui/icons-material";
import { Item } from "../search/types";
import { Action } from "../reducer/Action";

type Properties = {
	item: Item;
	starredMap: Map<number, Item>;
	dispatch: Dispatch<Action>;
};

function SingleItem(props: Properties) {
	const { item, starredMap, dispatch } = props;
	const { html_url, name, description, full_name, stargazers_count, id } = item;

	const onStarClick = useCallback(() => {
		dispatch({ type: "TOGGLE_STARRED", items: [item] });
	}, [dispatch, item]);

	const isStarred = starredMap.get(id);
	return (
		<ListItem
			secondaryAction={
				<Button
					variant="contained"
					startIcon={isStarred ? <Star /> : <StarBorder />}
					onClick={onStarClick}
				>
					{stargazers_count + (isStarred ? 1 : 0)}
				</Button>
			}
		>
			<ListItemButton component="a" href={html_url}>
				<ListItemText primary={name} secondary={description || full_name} />
			</ListItemButton>
		</ListItem>
	);
}

export default SingleItem;
