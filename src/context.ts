import { createContext, Dispatch } from "react";
import { Action } from "./reducer/Action";
import { initialState } from "./reducer/State";

const defaultContext = {
	state: initialState,
	dispatch: (() => {}) as Dispatch<Action>,
};

export const context = createContext(defaultContext);
