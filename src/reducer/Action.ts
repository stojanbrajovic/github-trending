import { Item, Languages, SearchParams } from "../search/types";

export type Action =
	| {
			type: "SET_SEARCH_RESULTS";
			items: Item[];
			totalCount: number;
	  }
	| {
			type: "TOGGLE_STARRED";
			items: Item[];
	  }
	| {
			type: "CHANGE_SEARCH_PARAMS";
			params: Partial<SearchParams>;
	  }
	| { type: "ASYNC_ACTION_START" }
	| { type: "ASYNC_ACTION_END" }
	| {
			type: "SET_AVAILABLE_LANGUAGES";
			languages: Languages;
	  };
