import { Item, Languages, SearchParams } from "../search/types";

export const initialState = {
	asyncActionsActive: 0,
	items: [] as Item[],
	totalCount: 0 as number,
	starredMap: new Map<number, Item>(),
	searchParams: {
		page: 1,
		pageSize: 10,
		filter: "",
		showStarred: false,
		language: "",
	} as SearchParams,
	availableLanguages: {} as Languages,
};

export type State = typeof initialState;
