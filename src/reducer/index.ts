import { Action } from "./Action";
import { initialState, State } from "./State";

const reducer = (state = initialState, action: Action): State => {
	switch (action.type) {
		case "TOGGLE_STARRED": {
			const { items } = action;
			const newMap = new Map(state.starredMap);
			items.forEach((item) => {
				const { id } = item;
				newMap.get(id) ? newMap.delete(id) : newMap.set(id, item);
			});
			return { ...state, starredMap: newMap };
		}
		case "SET_SEARCH_RESULTS": {
			return { ...state, items: action.items, totalCount: action.totalCount };
		}
		case "CHANGE_SEARCH_PARAMS": {
			return {
				...state,
				searchParams: { ...state.searchParams, ...action.params },
			};
		}
		case "ASYNC_ACTION_START": {
			return { ...state, asyncActionsActive: state.asyncActionsActive + 1 };
		}
		case "ASYNC_ACTION_END": {
			return { ...state, asyncActionsActive: state.asyncActionsActive - 1 };
		}
		case "SET_AVAILABLE_LANGUAGES": {
			return { ...state, availableLanguages: action.languages };
		}
	}

	console.error("Unhandled action", action);
};

export default reducer;
