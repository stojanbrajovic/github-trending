import { SearchParams } from "../types";
import getUrlParams from "../getUrlParams";

describe("getUrlParams", () => {
	const defaultParams: SearchParams = {
		filter: "",
		language: "",
		page: 1,
		pageSize: 10,
		showStarred: false,
	};

	beforeAll(() => {
		jest.useFakeTimers("modern");
		jest.setSystemTime(new Date(2020, 0, 8));
	});

	afterAll(() => {
		jest.useRealTimers();
	});

	it("should return date seven days prior to mocked data and page params", () => {
		const result = getUrlParams(defaultParams);
		expect(result).toBe("q=created:>2020-1-1&per_page=10&page=1");
	});

	it("should return URL with filter", () => {
		const result = getUrlParams({ ...defaultParams, filter: "test" });
		expect(result).toBe("q=test&created:>2020-1-1&per_page=10&page=1");
	});

	it("should return URL with language set", () => {
		const result = getUrlParams({ ...defaultParams, language: "testLang" });
		expect(result).toBe(
			"q=language=testLang&created:>2020-1-1&per_page=10&page=1",
		);
	});

	it("showStarred should not affect url", () => {
		const result = getUrlParams({ ...defaultParams, showStarred: true });
		expect(result).toBe("q=created:>2020-1-1&per_page=10&page=1");
	});

	it("should return both filter and language filter", () => {
		const result = getUrlParams({
			...defaultParams,
			language: "testLang",
			filter: "testFilter",
		});
		expect(result).toBe(
			"q=testFilter&language=testLang&created:>2020-1-1&per_page=10&page=1",
		);
	});

	it("should return url with different page params", () => {
		const result = getUrlParams({ ...defaultParams, pageSize: 100, page: 5 });
		expect(result).toBe("q=created:>2020-1-1&per_page=100&page=5");
	});
});
