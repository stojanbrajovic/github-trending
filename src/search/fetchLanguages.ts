import { load } from "js-yaml";
import { Languages } from "./types";

const fetchLanguages = async (): Promise<Languages> => {
	const response = await fetch(
		"https://raw.githubusercontent.com/github/linguist/master/lib/linguist/languages.yml",
	);
	const blob = await response.blob();
	const text = await blob.text();
	const languagesObject = load(text) as Languages;

	return languagesObject;
};

export default fetchLanguages;
