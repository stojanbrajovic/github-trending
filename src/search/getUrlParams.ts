import { SearchParams } from "./types";

const getDateStringWeekAgo = () => {
	const date = new Date();
	date.setDate(date.getDate() - 7);
	return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
};

const getUrlParams = (params: SearchParams) => {
	const { filter, language } = params;

	const urlParams = [];
	if (filter) {
		urlParams.push(encodeURIComponent(filter));
	}

	if (language) {
		urlParams.push(`language=${language}`);
	}

	urlParams.push(`created:>${getDateStringWeekAgo()}`);
	urlParams.push(`per_page=${params.pageSize}`);
	urlParams.push(`page=${params.page}`);

	return `q=${urlParams.join("&")}`;
};

export default getUrlParams;
