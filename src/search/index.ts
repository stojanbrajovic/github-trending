import getUrlParams from "./getUrlParams";
import { Item, Result, SearchParams } from "./types";

export const searchStarred = (
	params: SearchParams,
	starred: Map<number, Item>,
): Result => {
	const { filter, pageSize, page } = params;
	const allStarredItems = Array.from(starred, ([_id, item]) => item).filter(
		({ name }) => name.includes(filter),
	);
	allStarredItems.sort((i1, i2) => i2.stargazers_count - i1.stargazers_count);
	const sliceStart = (page - 1) * pageSize;
	const items = allStarredItems.slice(sliceStart, sliceStart + pageSize);

	return {
		items,
		total_count: allStarredItems.length,
	};
};

export const searchGithubApi = async (
	params: SearchParams,
): Promise<Result> => {
	const response = await fetch(
		`https://api.github.com/search/repositories?${getUrlParams(
			params,
		)}&sort=stars&order=desc`,
	);
	const result: Result = await response.json();
	return {
		...result,
		items:
			result.items?.map(
				({
					created_at,
					description,
					full_name,
					html_url,
					id,
					name,
					stargazers_count,
				}) => ({
					created_at,
					description,
					full_name,
					html_url,
					id,
					name,
					stargazers_count,
				}),
			) || [],
	};
};
