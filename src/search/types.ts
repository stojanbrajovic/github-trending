export type Item = {
	id: number;
	name: string;
	full_name: string;
	html_url: string;
	created_at: string;
	description: string;
	stargazers_count: number;
};

export type Result = {
	total_count: number;
	items: Item[];
};

export type SearchParams = {
	pageSize: number;
	page: number;
	filter: string;
	showStarred: boolean;
	language: string;
};

export type Languages = Record<
	string,
	{
		color: string;
		language_id: number;
	}
>;
